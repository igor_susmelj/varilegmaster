ADD_FILES = src/serialHelper.cpp
ADD_LIBRARIES = -lzmq -lserial

PTI = ../varileginterface/
ADD_INTERFACE = ${PTI}Commands_Datenpacket.cpp ${PTI}IDatenPacket.cpp ${PTI}Intercom_datenpacket.cpp ${PTI}Output_Datenpacket.cpp ${PTI}Packet_Reciever.cpp ${PTI}Sensor_Datenpacket.cpp

all: directories main main_debug

directories:
	mkdir -p build/release build/debug

main: src/main.cpp
	g++ -Wall -O3 --std=c++11 -o build/release/master ${ADD_FILES} ${ADD_INTERFACE} src/main.cpp ${ADD_LIBRARIES}



main_debug: src/main.cpp
	g++ -Wall -O3 -g --std=c++11 -DDEBUG -o build/debug/master ${ADD_FILES} ${ADD_INTERFACE} src/main.cpp ${ADD_LIBRARIES}

clean:
	rm -f build/release/master build/debug/master

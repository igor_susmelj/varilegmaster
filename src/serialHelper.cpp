#include <SerialStream.h>

#include "serialHelper.hpp"



SerialHelper::SerialHelper(std::string filename)
{
	_filename = filename;
	//Initialize serial stream with LibSerial
	_serial_port.Open(_filename);
	
	sleep(1);
	
	//send magic byte to the slave
	char tmp = 'Z';
	write(&tmp, 1);
}

SerialHelper::~SerialHelper()
{
	//close serial port
	_serial_port.Close();
}

void SerialHelper::write(const char *buffer, int len)
{
	_serial_port.write(buffer , len);
}

void SerialHelper::read(char * buffer, int len)
{
	while(_serial_port.rdbuf()->in_avail() > 0)
	{
		_serial_port.read(buffer, len);

		//check if problem with connection and reset if neccessairy
		if(_serial_port.rdbuf()->in_avail() > 0)
		{
			_serial_port.Close();
			_serial_port.Open(_filename);
		}
	}


}


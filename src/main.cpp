#include <zmq.hpp>
#include <string>
#include <iostream>
#include <unistd.h>
#include <random>

#include <chrono>

//include boost libraries
//#include <boost/program_options.hpp>

//include interface files
#include "../../varileginterface/Commands_Datenpacket.h"
#include "../../varileginterface/IDatenPacket.h"
#include "../../varileginterface/Intercom_datenpacket.h"
#include "../../varileginterface/Output_Datenpacket.h"
#include "../../varileginterface/Packet_Reciever.h"
#include "../../varileginterface/Sensor_Datenpacket.h"

//include custom files
#include "monitorHelper.hpp"
#include "monitorCommunication.hpp"
#include "serialHelper.hpp"
#include "data_interface_slave.h"
#include "conversionHelper.hpp"
#include "defines.h"


#define BUF 16				//size of buffer in floats (4 bytes)
#define POLL_TIMEOUT 10		//us timeout for polling
#define MASTER_SLEEP 1			//s sleep for master


using namespace std;


//global variables
bool last_button_states[6] = {0};

//functions
void process_input_arguments(int argc, char*argv[]);
void handle_user_input(Slave_Data *slave_data, zmq::socket_t *calc_cocket);
bool get_bit(uint16_t val, int offset);


int main(int argc, char *argv[]){


	//set cout precision for better readability
	std::cout.precision(3);


	#ifdef DEBUG
		cout << "Starting with initialization..." << endl;
	#endif

	//Initialize vairiables
	Slave_Data slave_1_data = {0};
	Slave_Data slave_2_data = {0};
	Slave_Data slave_3_data = {0};

	Master_Data master_1_data = {0};
	Master_Data master_2_data = {0};
	Master_Data master_3_data = {0};

	//Initialize serial COM port with slaves
	SerialHelper slave_1_serial("/dev/ttyACM0");
	SerialHelper slave_2_serial("/dev/ttyACM1");
	SerialHelper slave_3_serial("/dev/ttyACM2");

	#ifdef DEBUG
		cout << "Initialized serial communication" << endl;
	#endif


	//Initialize data packets
	Sensor_Datenpacket *sensor_data = new Sensor_Datenpacket(1);
	Commands_Datenpacket *command_data = new Commands_Datenpacket(1);		
	Packet_Reciever rec;

 	//Initialize sockets
	zmq::context_t context(1);
	zmq::socket_t monitor_socket(context,ZMQ_PAIR);	
	monitor_socket.bind("tcp://*:5556");
	zmq::socket_t calculation_socket(context, ZMQ_PAIR);
	calculation_socket.bind("tcp://*:5555");

	//Initialize polling
	zmq::pollitem_t poller [] = {
		{monitor_socket, 0, ZMQ_POLLIN, 0}, 
		{calculation_socket, 0, ZMQ_POLLIN, 0}
	};
	
	#ifdef DEBUG
		cout << "Initialized sockets" << endl;
	#endif


	chrono::time_point<chrono::system_clock> t1, t2, t3;
	t3 = chrono::system_clock::now();

	//Main loop
	while(true)
	{
		//start measure time
		t1 = std::chrono::system_clock::now();


		//===================HANDLE SLAVE STUFF=======================//
		
		
	
		//send master data package to slave
		slave_1_serial.write((const char*)&master_1_data, sizeof(master_1_data));		
		
		//receive slave data package from slave
		slave_1_serial.read((char *)&slave_1_data, sizeof(Slave_Data));
			

		//send master data package to slave
		slave_2_serial.write((const char*)&master_2_data, sizeof(master_2_data));		
		
		//receive slave data package from slave
		slave_2_serial.read((char *)&slave_2_data, sizeof(Slave_Data));
			
	
		//send master data package to slave
		slave_3_serial.write((const char*)&master_3_data, sizeof(master_3_data));		
		
		//receive slave data package from slave
		slave_3_serial.read((char *)&slave_3_data, sizeof(Slave_Data));
			
		
		switch(slave_1_data.ID)
		{
			case 1:
				transfer_data(&slave_1_data, sensor_data, TD_IMU1);	
				transfer_data(&slave_1_data, sensor_data, TD_IMU2);	
				transfer_data(&slave_1_data, sensor_data, TD_SOFT_POT0);
				transfer_data(&slave_1_data, sensor_data, TD_SOFT_POT1);
				transfer_data(&slave_1_data, sensor_data, TD_SOFT_POT2);
				break;
			case 2:
				transfer_data(&slave_1_data, sensor_data, TD_IMU3);	
				transfer_data(&slave_1_data, sensor_data, TD_HIP_POT0);
				transfer_data(&slave_1_data, sensor_data, TD_HIP_POT1);

				//send triggers of hardware buttons
				handle_user_input(&slave_1_data, &calculation_socket);
				break;
			case 3:
				transfer_data(&slave_1_data, sensor_data, TD_IMU4);	
				transfer_data(&slave_1_data, sensor_data, TD_IMU5);
				transfer_data(&slave_1_data, sensor_data, TD_SOFT_POT3);
				transfer_data(&slave_1_data, sensor_data, TD_SOFT_POT4);
				transfer_data(&slave_1_data, sensor_data, TD_SOFT_POT5);
				break;
		}
		
		switch(slave_2_data.ID)
		{
			case 1:
				transfer_data(&slave_2_data, sensor_data, TD_IMU1);	
				transfer_data(&slave_2_data, sensor_data, TD_IMU2);	
				transfer_data(&slave_2_data, sensor_data, TD_SOFT_POT0);
				transfer_data(&slave_2_data, sensor_data, TD_SOFT_POT1);
				transfer_data(&slave_2_data, sensor_data, TD_SOFT_POT2);
				break;
			case 2:
				transfer_data(&slave_2_data, sensor_data, TD_IMU3);	
				transfer_data(&slave_2_data, sensor_data, TD_HIP_POT0);
				transfer_data(&slave_2_data, sensor_data, TD_HIP_POT1);

				//send triggers of hardware buttons
				handle_user_input(&slave_2_data, &calculation_socket);
				break;
			case 3:
				transfer_data(&slave_2_data, sensor_data, TD_IMU4);	
				transfer_data(&slave_2_data, sensor_data, TD_IMU5);	
				transfer_data(&slave_2_data, sensor_data, TD_SOFT_POT3);
				transfer_data(&slave_2_data, sensor_data, TD_SOFT_POT4);
				transfer_data(&slave_2_data, sensor_data, TD_SOFT_POT5);
				break;
		}
		
		switch(slave_3_data.ID)
		{
			case 1:
				transfer_data(&slave_3_data, sensor_data, TD_IMU1);	
				transfer_data(&slave_3_data, sensor_data, TD_IMU2);	
				transfer_data(&slave_3_data, sensor_data, TD_SOFT_POT0);
				transfer_data(&slave_3_data, sensor_data, TD_SOFT_POT1);
				transfer_data(&slave_3_data, sensor_data, TD_SOFT_POT2);
				break;
			case 2:
				transfer_data(&slave_3_data, sensor_data, TD_IMU3);	
				transfer_data(&slave_3_data, sensor_data, TD_HIP_POT0);
				transfer_data(&slave_3_data, sensor_data, TD_HIP_POT1);
				
				//send triggers of hardware buttons
				handle_user_input(&slave_3_data, &calculation_socket);
				break;
			case 3:
				transfer_data(&slave_3_data, sensor_data, TD_IMU4);	
				transfer_data(&slave_3_data, sensor_data, TD_IMU5);	
				transfer_data(&slave_3_data, sensor_data, TD_SOFT_POT3);
				transfer_data(&slave_3_data, sensor_data, TD_SOFT_POT4);
				transfer_data(&slave_3_data, sensor_data, TD_SOFT_POT5);
				break;
		}


		//===================HANDLE IPC STUFF=========================//

		zmq_send(sensor_data->convertToByte(), &calculation_socket);
		
		//wait POLL_TIMEOUT microseconds for input messages
		zmq::poll(&poller[0],2 , POLL_TIMEOUT);
		
		//check input messages
		if(poller[1].revents & ZMQ_POLLIN)	//check if message from calculation process arrived
		{
			//handle mesage
			zmq::message_t request;
			calculation_socket.recv(&request);
			vector<unsigned char> msg(request.size());
			memcpy((void *)msg.data(), request.data(), request.size());
			if(msg[0] == 3)
			{
				*command_data = rec.reciveComand(msg);	
			}
		}

		//===================Send Data to Slave=========================//
		


		switch(slave_1_data.ID)
		{
			case 1:
				transfer_commands(&master_1_data, command_data, SLAVE1);	
				break;
			case 2:
				transfer_commands(&master_1_data, command_data, SLAVE2);
				break;
			case 3:			
				transfer_commands(&master_1_data, command_data, SLAVE3);
				break;
		}
		
		switch(slave_2_data.ID)
		{
			case 1:
				transfer_commands(&master_2_data, command_data, SLAVE1);	
				break;
			case 2:
				transfer_commands(&master_2_data, command_data, SLAVE2);
				break;
			case 3:
				transfer_commands(&master_2_data, command_data, SLAVE3);	
				break;
		}
		
		switch(slave_3_data.ID)
		{
			case 1:
				transfer_commands(&master_3_data, command_data, SLAVE1);	
				break;
			case 2:
				transfer_commands(&master_3_data, command_data, SLAVE2);
				break;
			case 3:
				transfer_commands(&master_3_data, command_data, SLAVE3);	
				break;
		}
	
		
		
		//===================HANDLE MONITOR STUFF====================//

		if(poller[0].revents & ZMQ_POLLIN)	//check if message from monitor process arrived
		{
			//handle message
			zmq::message_t request;
			monitor_socket.recv(&request);

			//Forward messages to calculation
			vector<unsigned char> msg(request.size());
			memcpy((void *)msg.data(), request.data(), request.size());
			zmq_send(msg, &calculation_socket);


			#ifdef DEBUG
			std::string dbg_msg = std::string(static_cast<char*>(request.data()), request.size());	
			cout << "Received from monitor server: " << dbg_msg << endl;
			#endif
		}


	
		#if SEND_TO_MONITOR_ON
			zmq_send(sensor_data->convertToByte(), &monitor_socket);
			zmq_send(command_data->convertToByte(), &monitor_socket);
		#endif
		

		//end measure time
		t2 = std::chrono::system_clock::now();
	

		chrono::duration<double> time_span_print = (t2-t3);
		chrono::duration<double> time_span = (t2-t1);

		t3 = t2;

		cout << "Master-slave cycle time: " << time_span_print.count() << " s" << endl;

		//usleep(200*1000-(int)(time_span.count()*1000000));
		usleep(50*1000);
	}

	//clean up
	slave_1_serial.~SerialHelper();
	slave_2_serial.~SerialHelper();
	slave_3_serial.~SerialHelper();


	return 0;
}


void handle_user_input(Slave_Data *slave_data, zmq::socket_t *calc_cocket)
{
	//std::cout << "slave button data: " << slave_data->USER_INPUT << std::endl;

	//set all triggers to appropirate value
	if(get_bit(slave_data->USER_INPUT,0) == 1 && last_button_states[0] == 0)
	{
		//std::cout << "trigger button 1" << std::endl;
		Intercom_datenpacket *intercom_data = new Intercom_datenpacket();	
		intercom_data->com[0] = 20;
		intercom_data->data[0] = 1;
		zmq_send(intercom_data->convertToByte(), calc_cocket);
		delete intercom_data;
		last_button_states[0] = 1;
	}else if(last_button_states[0] == 1)
	{
		last_button_states[0] = 0;
	}
	if(get_bit(slave_data->USER_INPUT,1) == 1 && last_button_states[1] == 0)
	{
		//std::cout << "trigger button 2" << std::endl;
		Intercom_datenpacket *intercom_data = new Intercom_datenpacket();	
		intercom_data->com[0] = 21;
		intercom_data->data[0] = 1;
		zmq_send(intercom_data->convertToByte(), calc_cocket);
		delete intercom_data;
		last_button_states[1] = 1;
	}else if(last_button_states[1] == 1)
	{
		last_button_states[1] = 0;
	}
	if(get_bit(slave_data->USER_INPUT,2) == 1  && last_button_states[2] == 0)
	{
		//std::cout << "trigger button 3" << std::endl;
		Intercom_datenpacket *intercom_data = new Intercom_datenpacket();	
		intercom_data->com[0] = 22;
		intercom_data->data[0] = 1;
		zmq_send(intercom_data->convertToByte(), calc_cocket);
		delete intercom_data;
		last_button_states[2] = 1;
	}else if(last_button_states[2] == 1)
	{
		last_button_states[2] = 0;
	}
	if(get_bit(slave_data->USER_INPUT,3) == 1  && last_button_states[2] == 0)
	{
		//std::cout << "trigger button 4" << std::endl;
		Intercom_datenpacket *intercom_data = new Intercom_datenpacket();	
		intercom_data->com[0] = 23;
		intercom_data->data[0] = 1;
		zmq_send(intercom_data->convertToByte(), calc_cocket);
		delete intercom_data;
		last_button_states[3] = 1;
	}else if(last_button_states[3] == 1)
	{
		last_button_states[3] = 0;
	}
}


bool get_bit(uint16_t val, int offset)
{
	return (val & (1 << offset)) >> offset;
}
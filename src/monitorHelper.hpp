#ifndef MONITORHELPER_H
#define MONITORHELPER_H

#include <iostream>
#include <random>

//initialize random device
std::random_device rd;

//initialize random distribution big and small
std::uniform_real_distribution<float> dist_small(-1,1);
std::uniform_real_distribution<float> dist_big(-15,15);

//get random number, big or small
inline float get_random_number(bool big)
{
	return big ? dist_big(rd) : dist_small(rd);
}

//fill an array with random numbers
void fill_with_data(float *buffer, int offset, int size, bool big)
{
	if(big)
		for(int i = offset; i < offset + size; ++i)
			buffer[i] = get_random_number(big);
	else
		for(int i = offset; i < offset + size; ++i)
			buffer[i] = get_random_number(big);
}

//print buffer for logging
void print_data(float *buffer, int size)
{
	std::cout << "Current data from compute node: " << std::endl;
	for(int i = 0; i < size; ++i)
		std::cout << buffer[i] << std::endl;
}

#endif

#ifndef __SERIAL_HELPER_H__
#define __SERIAL_HELPER_H__

#include <SerialStream.h>
#include <string.h>
#include <unistd.h>
#include "data_interface_slave.h"

using namespace LibSerial;

class SerialHelper
{
	public:
		SerialHelper(std::string filename);
		~SerialHelper();
		void write(const char *buffer, int len);
		void read(char *buffer, int len);

	private:
		//variables for slave communication
		SerialStream _serial_port;
		std::string _filename;
};

#endif
